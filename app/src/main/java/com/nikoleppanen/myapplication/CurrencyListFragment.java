package com.nikoleppanen.myapplication;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class CurrencyListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    OnCurrencySelectedListener mCallBack;
    private CursorAdapter adapter;
    private static final int LOADER_ID = 0x02;

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), Uri.parse("content://com.nikoleppanen.myapplication.MyContentProvider/currencies")
                ,new String [] {"_id", "name", "relation", "date"}, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        adapter.swapCursor(cursor);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    public interface OnCurrencySelectedListener {

        public void onCurrencySelected(long id);
    }

    @Override
    public void onStart() {
        super.onStart();

        //CHECK MAIN ACTIVITY FOR IMPLEMENTATION
        Activity activity = getActivity();
        try {
            mCallBack = (OnCurrencySelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCurrencySelectedListener");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(LOADER_ID, null, this);

        int layout = android.R.layout.simple_list_item_activated_2;
        adapter = new SimpleCursorAdapter(getActivity(),
                layout,
                null,
                new String[] { "name", "relation", "date"},
                new int[] { android.R.id.text1, android.R.id.text2 }, 0);
        setListAdapter(adapter);

    }

    @Override
    public void onListItemClick(android.widget.ListView l, View v, int position, long id){

        //CALLBACK TO MAIN ACTIVITY
        mCallBack.onCurrencySelected(id);
        getListView().setItemChecked(position, true);
        getActivity().getSupportFragmentManager().popBackStack();
    }

}
