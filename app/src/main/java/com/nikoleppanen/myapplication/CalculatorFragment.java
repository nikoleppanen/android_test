package com.nikoleppanen.myapplication;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

/**
 * A placeholder fragment containing a simple view.
 */
public class CalculatorFragment extends Fragment{

    Bundle bundle;
    TextView mUpperCur, mLowerCur;
    EditText mUpperInput, mLowerInput;


    public CalculatorFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        bundle = this.getArguments();
        return inflater.inflate(R.layout.fragment_calculator, container, false);

    }

    @Override
    public void onStart(){
        super.onStart();
        if(this.getArguments() != null){
            mLowerCur = (TextView)getView().findViewById(R.id.lowerCur);
            mLowerCur.setText(bundle.getString("name"));
        }

    }

    public void onCalculate(View view){
        if(this.getArguments() != null){
            Toast.makeText(getActivity(), "Calculating!", Toast.LENGTH_SHORT).show();
            double rate = Double.parseDouble(bundle.getString("relation"));
            mUpperInput = (EditText)getView().findViewById(R.id.upperInput);
            double result = rate * Double.parseDouble(mUpperInput.getText().toString());
            mLowerInput = (EditText)getView().findViewById(R.id.lowerInput);
            mLowerInput.setText(String.format(Locale.getDefault(), "%.2f", result), TextView.BufferType.EDITABLE);
        } else {
            Toast.makeText(getActivity(), "Calculating!", Toast.LENGTH_SHORT).show();
            double rate = 1.0513;
            mUpperInput = (EditText)getView().findViewById(R.id.upperInput);
            double result = rate * Double.parseDouble(mUpperInput.getText().toString());
            mLowerInput = (EditText)getView().findViewById(R.id.lowerInput);
            mLowerInput.setText(String.format(Locale.getDefault(), "%.2f", result), TextView.BufferType.EDITABLE);
        }
    }
}
