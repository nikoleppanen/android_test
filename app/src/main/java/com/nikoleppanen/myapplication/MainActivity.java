package com.nikoleppanen.myapplication;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements CurrencyListFragment.OnCurrencySelectedListener{

    Bundle bundle = new Bundle();


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            //DATABASE INITIALISATION
            /*db = new DBAdapter(this);
            initDatabase();
            readDatabase(1);*/

            // create Calculator-fragment programmatically...
            CalculatorFragment newFragment = new CalculatorFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.add(R.id.fragment_container, newFragment);
            //transaction.addToBackStack(null); // to get back from ListFragment
            // Commit the transaction
            transaction.commit();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        // Configure the search info and add any event listeners...
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                Toast.makeText(getApplicationContext(), "SearchOnQueryTextSubmit: " + query, Toast.LENGTH_SHORT)
                        .show();

                if( ! searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                searchItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if (id==R.id.action_list){

            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            CurrencyListFragment listFragment = new CurrencyListFragment();
            transaction.addToBackStack(null);
            transaction.replace(R.id.fragment_container, listFragment)
                    .commit();

        }else if(id == R.id.action_add){

            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            AddCurrencyFragment addFragment = new AddCurrencyFragment();
            transaction.addToBackStack(null);
            transaction.replace(R.id.fragment_container, addFragment)
                    .commit();

        }else if(id == R.id.action_start){
            startService(new Intent(this, MyService.class));
            return true;

        }else if(id == R.id.action_stop){
            stopService(new Intent(this, MyService.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //CALCULATE FOR MAIN ACTIVITY
    public void onCalculate(View view){
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.fragment_container);
        if (fragment!=null)
            ((CalculatorFragment)fragment).onCalculate(view); // välitetään näppäinpainallukset fragmentille...
    }

    /*public void DisplayTitle(Cursor c){

        Toast.makeText(this,
                "id: " + c.getString(0) + "\n" +
                        "Name: " + c.getString(1) + "\n" +
                        "Relation: " + c.getString(2) + "\n" +
                        "Date:  " + c.getString(3),
                Toast.LENGTH_LONG).show();
    }

    private void initDatabase(){ // hard coded currency names at this point...

        //---add 3 titles---
        db.open();
        long id;
        id = db.insertCurrency(
                "USD",
                "1.0513", // 1 euro = 1,0513 dollars
                "26.02.2017");
        id = db.insertCurrency(
                "GBP",
                "0.844",
                "26.02.2017");
        id = db.insertCurrency(
                "SEK",
                "9.47",
                "26.02.2017");
        db.close();
    }

    private void readDatabase(int rowId){ // reads the base with  rowId sets the currency name...

        db.openForReading();
        c = db.getTitle(rowId);

        if (c.moveToFirst()){

            DisplayTitle(c);
            String Name = c.getString(1); // Name of a currency
            //String Rate = c.getString(2); // Exchange Rate

        }else{

            Toast.makeText(this, "No currency found", Toast.LENGTH_SHORT).show();
        }
        db.close();

        //GET CURRENCY DATA FROM CURSOR TO BUNDLE
        bundle.putString("id", c.getString(0));
        bundle.putString("name", c.getString(1));
        bundle.putString("relation", c.getString(2));
        bundle.putString("date", c.getString(3));

        //START NEW CALCULATOR FRAGMENT WITH BUNDLE
        CalculatorFragment new_fragment = new CalculatorFragment();
        new_fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().addToBackStack(null).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new_fragment).commit();

    }*/

    @Override
    public void onCurrencySelected(long id) {

        //QUERY CONTENT PROVIDER WITH CURRENCY ROW ID
        Cursor mCursor = getContentResolver().query(Uri.parse("content://com.nikoleppanen.myapplication.MyContentProvider/currencies"), new String[]{"name", "relation"}, "_id = ?", new String[]{String.valueOf(id)}, null);
        mCursor.moveToFirst();
        Toast.makeText(this, "Activity item selected: " + mCursor.getString(0) + " : " + mCursor.getString(1) , Toast.LENGTH_SHORT).show();

        //GET CURRENCY DATA FROM CURSOR TO BUNDLE
        bundle.putString("name", mCursor.getString(0));
        bundle.putString("relation", mCursor.getString(1));

        //START NEW CALCULATOR FRAGMENT WITH BUNDLE
        CalculatorFragment new_fragment = new CalculatorFragment();
        new_fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().addToBackStack(null).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new_fragment).commit();

    }
}
