package com.nikoleppanen.myapplication;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddCurrencyFragment extends Fragment implements View.OnClickListener {

    Bundle bundle;
    EditText mInputName, mInputRate, mInputDate;

    public AddCurrencyFragment(){
        //EMPTY CONSTRUCTOR
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        bundle = this.getArguments();
        View v = inflater.inflate(R.layout.fragment_add, container, false);
        Button b = v.findViewById(R.id.add_currency_button);
        b.setOnClickListener(this);
        return v;

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.add_currency_button){
            ContentValues values = new ContentValues();
            mInputName = getView().findViewById(R.id.input_currency_code);
            values.put("name", mInputName.getText().toString());
            mInputRate = getView().findViewById(R.id.input_currency_rate);
            values.put("relation", Double.parseDouble(mInputRate.getText().toString()));
            mInputDate = getView().findViewById(R.id.input_currency_date);
            values.put("date", mInputDate.getText().toString());
            Uri uri = getActivity().getContentResolver().insert(Uri.parse("content://com.nikoleppanen.myapplication.MyContentProvider/currencies"), values);
            if(uri != null){
                Toast.makeText(getContext(), "Added successfully! Uri: " + uri.toString(), Toast.LENGTH_SHORT).show();
                getActivity().getSupportFragmentManager().popBackStack();
            }else{
                Toast.makeText(getContext(), "Failed to add currency", Toast.LENGTH_SHORT).show();
            }
        }

    }
}
